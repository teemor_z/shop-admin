$(function () {})

function imgFormatter(index, value, row) {
  return "<img width='100px'  src='" + value + "'></img>";
}

var app = new Vue({
  el: "#content",
  data() {
    return {
      tableData: []
    }
  },
  mounted() {
    this.initOrders();
  },
  methods: {
    initOrders() {
      baseAxios.get("/trade/order/orders?orderStates=1&orderStates=3&orderStates=5", {
        params: {
          mchtId:-1
        }
      }).then((res) => {
        var result = res.data;
        if (result.code == 200) {
          this.tableData = result.data;
        } else {
          this.tableData = [];
        }
      })
    },
    orderStatusFormatter(state) {
      switch (state) {
        case 0:
          return "待提交"
        case 1:
          return "已提交"
        case 2:
          return "待支付"
        case 3:
          return "已支付"
        case 4:
          return "已取消"
        case 11:
          return "已确认"
        case 12:
          return "卖家取消"
        case 13:
          return "待收货"
        case 14:
          return "待评价"
        case 15:
          return "已完成"
      }
    },
    checkOrder(id, state, type) {
      this.$prompt('请输入审核意见', '提示', {
        confirmButtonText: '确认',
        cancelButtonText: '取消'
      }).then(({
        value
      }) => {
        baseAxios.put("/trade/order/check", {},{
          params:{
            idStr: id,
            state: state,
            type: type,
            comment: value
          }
        }).then((res) => {
          this.initOrders();
          var result = res.data;
          if (result.code == 200) {
            this.$message({
              type: 'success',
              message: '操作成功'
            });
          } else {
            this.$message({
              type: 'info',
              message: result.msg
            });
          }
        })
      }).catch(({
        value
      }) => {

      });
    }
  },
})