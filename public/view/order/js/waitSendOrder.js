$(function () {})

function imgFormatter(index, value, row) {
  return "<img width='100px'  src='" + value + "'></img>";
}

var app = new Vue({
  el: "#content",
  data() {
    return {
      tableData: []
    }
  },
  mounted() {
    this.initOrders();
  },
  methods: {
    initOrders() {
      baseAxios.get("/trade/order/orders", {
        params: {
          orderStates: 11,
          mchtId:-1
        }
      }).then((res) => {
        var result = res.data;
        if (result.code == 200) {
          this.tableData = result.data;
        } else {
          this.tableData = [];
        }
      })
    },
    orderStatusFormatter(state) {
      switch (state) {
        case 0:
          return "待提交"
        case 1:
          return "已提交"
        case 2:
          return "待支付"
        case 3:
          return "已支付"
        case 4:
          return "已取消"
        case 11:
          return "已确认"
        case 12:
          return "卖家取消"
        case 13:
          return "待收货"
        case 14:
          return "待评价"
        case 15:
          return "已完成"
      }
    },
    submitExpress(orderId, tradeType) {
      if (tradeType == 1) {
        this.$prompt('请输入快递单号', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消'
        }).then(({
          value
        }) => {
          baseAxios.put("/trade/order/express", {}, {
            params: {
              idStr: orderId,
              expressNo: value
            }
          }).then((res) => {
            var result = res.data;
            if (result.code == 200) {
              this.initOrders();
              this.$message({
                type: 'success',
                message: '操作成功'
              });
            } else {
              this.$message({
                type: 'info',
                message: result.msg
              });
            }
          })
        });
      } else {
        baseAxios.put("/trade/order/express", {}, {
          params: {
            idStr: orderId,
            expressNo: ""
          }
        }).then((res) => {
          var result = res.data;
          if (result.code == 200) {
            this.initOrders();
            this.$message({
              type: 'success',
              message: '操作成功'
            });
          } else {
            this.$message({
              type: 'info',
              message: result.msg
            });
          }
        })
      }

    }
  },
})