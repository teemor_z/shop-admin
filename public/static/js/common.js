$(function () {
    $(document).ready(function () {
        $('#head').load('/tmpl/header.html');
        $('#menu').load('/tmpl/left-menu.html');
    });
})
// var AUTHTOKEN = $.cookie("AUTHTOKEN");
let AUTHTOKEN = localStorage.getItem("AUTHTOKEN")
var myAxios = axios.create({
    baseURL: 'http://localhost',
});
var baseAxios = axios.create({
    baseURL: 'http://localhost:8000'
});

baseAxios.interceptors.request.use(
    config => {
      config.withCredentials = false
      config.timeout = 6000
      let token = localStorage.getItem('AUTHTOKEN')
      if (token) {
        config.headers['AUTHTOKEN'] = token
      }
      return config
    },
    error => {
      return Promise.reject(error)
    }
  )

/**
 * axios通用工具 start
 */
var userAxios = axios.create({
    baseURL: 'http://localhost:8000/user',
    headers: {
        'AUTHTOKEN': AUTHTOKEN
    }
});
// 添加请求拦截器
userAxios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});
// 添加响应拦截器
userAxios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    data = response.data;
    if (data.code == 100) {
        layer.alert('请登录后再操作', {
            skin: 'layui-layer-lan' //样式类名
                ,
            closeBtn: 0
        }, function () {
            window.location = "/index.html";
        });

    }
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});


var itemAxios = axios.create({
    baseURL: 'http://localhost:8000/item',
    headers: {
        'AUTHTOKEN': AUTHTOKEN
    }
});
// 添加响应拦截器
itemAxios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    data = response.data;
    if (data.code == 100) {
        layer.alert('请登录后再操作', {
            skin: 'layui-layer-lan' //样式类名
                ,
            closeBtn: 0
        }, function () {
            window.location = "/index.html";
        });

    }
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});


var tradeAxios = axios.create({
    baseURL: 'http://localhost:8000/trade',
    headers: {
        'AUTHTOKEN': AUTHTOKEN
    }
});
// 添加响应拦截器
tradeAxios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    data = response.data;
    if (data.code == 100) {
        layer.alert('请登录后再操作', {
            skin: 'layui-layer-lan' //样式类名
                ,
            closeBtn: 0
        }, function () {
            window.location = "/index.html";
        });

    }
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});

var actAxios = axios.create({
    baseURL: 'http://localhost:8000/activity',
    headers: {
        'AUTHTOKEN': AUTHTOKEN
    }
});
// 添加响应拦截器
actAxios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    data = response.data;
    if (data.code == 100) {
        layer.alert('请登录后再操作', {
            skin: 'layui-layer-lan' //样式类名
                ,
            closeBtn: 0
        }, function () {
            window.location = "/index.html";
        });

    }
    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});
/**
 * axios通用工具 end
 */

function add0(m) {
    return m < 10 ? '0' + m : m
}
layui.use(['layer', 'form'], function () {});

//获取url参数
function getUrlParam(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    if (window.location.hash.indexOf("?") < 0) {
        return null;
    }
    let r = window.location.hash.split("?")[1].match(reg);
    if (r != null) return decodeURIComponent(r[2]);
    return null;
}

//日期格式化
function dateFormat(shijianchuo) {
    //shijianchuo是整数，否则要parseInt转换
    var time = new Date(shijianchuo);
    var y = time.getFullYear();
    var m = time.getMonth() + 1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return y + '-' + add0(m) + '-' + add0(d) + ' ' + add0(h) + ':' + add0(mm) + ':' + add0(s);
}

function tmTable(conf) {
    this.conf = conf;
    this.table = {};
    this.createTable = function () {
        tbThis = this;
        var tableBox = new Vue({
            el: conf.el,
            data: {
                tableTmpl: ""
            },
            methods: {
                init: function () { //初始化表格父容器
                    _this = this;
                    myAxios.get("/tmpl/tableTmpl.html")
                        .then(function (res) {
                            _this.tableTmpl = res.data;
                            _this.$nextTick(function () {
                                tbThis.initTable();
                            })
                        })
                }
            }
        })
        tableBox.init();
    }
    this.reloadTable = function () {
        this.initTable.reload();
    }
    this.initTable = function () {
        tbThis = this;
        this.table = new Vue({ //初始化表格
            el: "#tables",
            data: {
                startIndex: 0, //页码列表第一个下标，从0开始
                pageCount: 3, //总页码数
                pageSize: "", //一页显示行数数
                flag: 1, //当前选中的页码在页码列表中的位置
                len: 5, //页码显示长度
                items: {}, //数据
                url: "", //获取数据路径
                type: "get", //请求方式
                params: "", //查询参数
                fields: {}, //列表显示字段
                options: {}, //操作列字段
                optionwidth: "250px",
                tablewidth: "100%", //表格宽度
                selectAll: false
            },
            methods: {
                init: function () { //初始化插件
                    var conf = tbThis.conf;
                    var len = conf.len;
                    if (len != null && len != undefined) {
                        len = len > 10 ? 10 : len;
                    }
                    if (conf.url == null || conf.url == undefined || conf.url == "") {
                        console.log("未填写数据获取路径");
                    }
                    this.url = conf.url;
                    this.pageSize = conf.pageSize != undefined ? conf.pageSize : 10;
                    this.params = conf.params;
                    this.fields = conf.fields;
                    this.options = conf.options;
                    this.optionWidth = conf.optionWidth;
                    this.tableWidth = conf.tableWidth;
                    if (conf.selectAll != undefined && conf.selectAll != null)
                        this.selectAll = conf.selectAll;
                    this.loadData();
                },
                loadData: function () {
                    _this = this;
                    this.params.pageIndex = this.startIndex + this.flag;
                    this.params.pageSize = this.pageSize;
                    if (this.type == "get" || this.type == "GET") {
                        itemAxios.get(this.url, {
                                params: _this.params
                            })
                            .then(function (req) {
                                _this.items = req.data.items;
                                count = req.data.count;
                                _this.pageCount = Math.ceil(count / _this.pageSize);
                                _this.len = _this.len > _this.pageCount ? _this.pageCount : _this.len;
                            })
                    } else if (this.type == "post" || this.type == "POST") {
                        itemAxios.post(this.url, _this.params)
                            .then(function (req) {
                                _this.items = req.data.items;
                                count = req.data.count;
                                _this.pageCount = Math.ceil(count / _this.pageSize);
                                _this.len = _this.len > _this.pageCount ? _this.pageCount : _this.len;
                            })
                    }
                },
                toMax: function () { //跳转到最后一页
                    this.startIndex = this.pageCount - this.len;
                    this.flag = this.len;
                    this.loadData();
                },
                toMin: function () { //跳转到第一页
                    this.startIndex = 0;
                    this.flag = 1;
                    this.loadData();
                },
                toPage: function (i) { //跳转到某一页
                    this.flag = i;
                    len = this.len;
                    mid = Math.ceil(len / 2);
                    if (this.flag > mid) {
                        this.startIndex += this.flag - mid;
                        if (this.startIndex > this.pageCount - len) {
                            this.startIndex = this.pageCount - len;
                        } else {
                            this.flag = mid;
                        }
                    } else {
                        this.startIndex -= mid - this.flag
                        if (this.startIndex < 0) {
                            this.startIndex = 0;
                        } else {
                            this.flag = mid;
                        }
                    }
                    this.loadData();
                },
                reload: function () {
                    this.loadData();
                }
            }
        })
        this.table.init();
        this.reload = function () {
            this.table.reload();
        }
    }
}