var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var fs = require("fs");
// 设置端口号
var ServerConf=require("./ServerConf");
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public/static')));
app.use(express.static(path.join(__dirname, 'public/view')));
app.use('/', indexRouter);
app.use('/users', usersRouter);

app.listen(80,()=>{
  console.log(`App listening at port 8080`)
});
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  // res.render('404.html', { title: 'Express' });
  res.sendFile( __dirname + "/public/view/" + "404.html" );
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
process.env.PORT=ServerConf.ServicePort;//设置端口号，不要占用了。
module.exports = app;
