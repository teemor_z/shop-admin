var table={};
$(function () {
        table = new tmTable({
        el: "#tableBox",
        url: "/items4P",
        len: 5,
        pageSize: 10,
        selectAll: false,
        params: {},
        fields: [{
                column: "图片",
                field: "mainImg",
                width: "60px",
                formatter: imgFormatter
            }, {
                column: "商品Id",
                field: "itemId",
                width: "100px"
            }, {
                column: "商品名",
                field: "itemName",
                width: "200px"
            }, {
                column: "分类",
                field: "ctgName",
                width: "100px"
            },
            {
                column: "创建时间",
                field: "createTime",
                width: "200px",
                formatter: itemDateFormatter
            },  {
                column: "商品状态",
                field: "status",
                width: "100px",
                formatter:itemStateFormatter
            }
        ],
        options: [{
            column: "编辑",
            field: "edit",
            width: "100px",
            formatter: function (index, value, row) {
                var html=""
                if(row.status==0){
                    //  html+= "<a class='am-btn am-btn-default am-btn-warning mr-5' style='font-size:12px' onclick='editItem("+row.itemId+")'>编辑</a>";
                    html+= "<a class='am-btn am-btn-default am-btn-default mr-5' style='font-size:12px' onclick='itemDetails("+row.itemId+")'>查看</a>";
                    html+= "<a class='am-btn am-btn-default am-btn-success mr-5' style='font-size:12px' onclick='upItem("+row.itemId+")'>上架</a>";
                    html+= "<a class='am-btn am-btn-default am-btn-danger mr-5' style='font-size:12px'  onclick='delItem("+row.itemId+")'>删除</a>";
                }else if(row.status==1){
                    html+= "<a class='am-btn am-btn-default am-btn-default mr-5' style='font-size:12px' onclick='itemDetails("+row.itemId+")'>查看</a>";
                    html+= "<a class='am-btn am-btn-default am-btn-success mr-5' style='font-size:12px' onclick='downItem("+row.itemId+")'>下架</a>";
                }
                return html;
            }
        }
    
    ]

    })
    table.createTable();
})
function editItem(itemId){
    window.location="/item/itemDetails.html?itemid="+itemId;
}

function itemDetails(itemId){
    window.location="/item/itemDetails.html?itemid="+itemId;
}

function upItem(itemId){
    itemAxios.put("/itemStatus",{
        itemId:itemId,
        status:1
    }).then(function(response){
        result=response.data;
        data=result.data;
        if(result.code==200){
            layer.msg("上架成功")
            table.reload();
        }else{
            layer.msg(result.msg);
        }
    })
}

function downItem(itemId){
    itemAxios.put("/itemStatus",{
        itemId:itemId,
        status:0
    }).then(function(response){
        result=response.data;
        data=result.data;
        if(result.code==200){
            layer.msg("下架成功")
            table.reload();
        }else{
            layer.msg(result.msg);
        }
    })
}
function delItem(itemId){
    itemAxios.delete("/item",{
        params:{
            itemIdStr:itemId
        }
    }).then(function(response){
        result=response.data;
        data=result.data;
        if(result.code==200){
            layer.msg("删除成功")
            table.reload();
        }else{
            layer.msg((result.msg==null||""?"删除失败":result.msg));
        }
    })
}

function imgFormatter(index, value, row) {
    return "<img width='50px'  src='" + value + "'></img>";
}

function itemDateFormatter(index, value, row) {
    return dateFormat(value);
}

function priceFormatter(index, value, row) {
    return "" + value + " 元";
}

function discountFormatter(index, value, row) {
    if (value == null || value == "" || value == undefined) {
        return "" + row.sellingPrice + " 元";
    } else {
        return "" + value + " 元";
    }
}

function itemNewFormatter(index, value, row) {
    switch (value) {
        case 1:
            return "全新";
        case 2:
            return "九成新";
        case 3:
            return "八成新";
        case 4:
            return "六至七成新";
        case 5:
            return "五成新及以下";
    }
}

function payTypeFormatter(index, value, row) {
    switch (value) {
        case 1:
            return "线下"
        case 2:
            return "支付宝"
        case 3:
            return "微信"
        case 12:
            return "线下|支付宝"
        case 13:
            return "线下|微信"
        case 23:
            return "支付宝|微信"
        case 123:
            return "线下|支付宝|微信"
        default:
            return "默认"
    }
}

function transportTypeFormatter(index, value, row) {
    switch (value) {
        case 1:
            return "自提"
        case 2:
            return "送货上门"
        case 3:
            return "邮寄"
        case 12:
            return "自提|送货上门"
        case 13:
            return "自提|邮寄"
        case 23:
            return "送货上门|邮寄"
        case 123:
            return "自提|送货上门|邮寄"
        default:
            return "默认"
    }
}

function itemStateFormatter(index, value, row) {
    switch (value) {
        case 0:
            return "待上架"
        case 1:
            return "已上架"

    }
}